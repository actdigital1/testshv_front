import React, { useEffect, useState, Component } from "react";
import { Button, Checkbox, Form, input, label } from 'semantic-ui-react';
import axios from 'axios';
import Select from 'react-select'
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';


export default function Create() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [birth, setBirth] = useState('');
    const [state, setState] = useState([]);
    const [city, setCity] = useState('');
    const [valueState, SetValueState] = useState({});
    const [valueCity, SetValueCity] = useState({});
    const [value, setValue] = useState({});
    const [newValue, setNewValue] = useState({});


    useEffect(() => {
        axios.get(`https://servicodados.ibge.gov.br/api/v1/localidades/estados`)
            .then((response) => {
                setState(response.data.map((item) => ({value: item, label: item.nome})));
            })
    }, [])

    const handleChange = (selectedOption) => {
        SetValueState(selectedOption);
        setCities(selectedOption);
    };

    const handleChangeCity = (selectedOption) => {
        SetValueCity(selectedOption);
    };

    const onclickSubmit = () => {  
        const data = {
            "Name": "Marcela",
            "LastName": "dsfd",
            "BirthDate": "04-03-1999",
            "State": "MT",
            "BirthCity": "Maceió"
          };
        

        axios.post(`https://localhost:7133/api/client`, data, {
            "Access-Control-Allow-Origin" : "https://localhost:7133",
            "Access-Control-Allow-Credentials" : "true",
            "Access-Control-Allow-Methods" : "GET, POST, OPTIONS",
            // "Access-Control-Allow-Headers" : "Origin, Content-Type, Accept",
            // "proxy": "https://localhost:7133", 
            // "Allow": "POST",
            "Content-type": "Application/json",
            // 'Access-Control-Allow-Origin' : '*',
            // 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
          })
            .then((response) => {
                console.log(response);
            }).catch((error) =>{
                console.log(error);
            });
    };

    const setCities = (selectedOption) => {
        console.log(`Option selected:`, selectedOption);
        axios.get(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedOption.value.sigla}/municipios`)
            .then((response) => {
                setCity(response.data.map((item) => ({value: item, label: item.nome})));
            })
    };

    return(
        <form class="ui form create-form">
            <div class="field">
                <label>Nome</label>
                <input type="text" name="first-name" onChange={(e) => setFirstName(e.target.value)} placeholder="Nome" />
            </div>
            <div class="field">
                <label>Sobrenome</label>
                <input type="text" name="last-name"  placeholder="Sobrenome" onChange={(e) => setLastName(e.target.value)} />
            </div>
            <div class="field">
                {/* <label>Nascimento</label> */}
                {/* <input type="text" name="last-name" placeholder="Data de Nascimento" onChange={(e) => setBirth(e.target.value)} /> */}
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DatePicker
                        label="Basic example"
                        value={value}
                        onChange={(newValue) => {
                        setValue(newValue);
                        }}
                        renderInput={(params) => <TextField {...params} />}
                    />
            </LocalizationProvider>
            </div>
            <div class="field">
                <label>Estado</label>
                <Select options={state} onChange={handleChange} />
            </div>
            <div class="field">
                <label>Município</label>
                {/* <input type="text" name="last-name" placeholder="Município" /> */}
                <Select defaultMenuIsOpen="" options={city} onChange={handleChangeCity} />
            </div>
            
            <button class="ui button" onClick={onclickSubmit} type="submit" >Criar</button>
        </form>
    );
}
