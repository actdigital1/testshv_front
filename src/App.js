import logo from './logo.svg';
import './App.css';
import Create from './components/Create';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Update from './components/update';
import Read from './components/Read';

function App() {  
  return (

    <Router>
        <div class="main">
          <h2 class="main-header">Teste prático Act Digital/SHV</h2>
          <div>
            <Routes path='/' >
              <Route path='getall' element={<Read />} />
              <Route path='create' element={<Create />} />
              <Route path='update' element={<Update />} />
            </Routes>
          </div>
        </div>
    </Router>
  );
}

export default App;
